package poc.config.managemnt.greeting;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static poc.config.managemnt.greeting.Properties.*;

@Slf4j
@Configuration
@EnableConfigurationProperties({TeanantsProperties.class, CommonConfigurations.class})
public class PropertiesConfiguration {

    @Bean
    @RefreshScope
    public Properties.TenantsPropertiesWrapper tenantsPropertiesWrapper(
            TeanantsProperties teanantsProperties, CommonConfigurations commonConfigurations) {
        return new TenantsPropertiesWrapper(teanantsProperties, commonConfigurations);

    }
}
