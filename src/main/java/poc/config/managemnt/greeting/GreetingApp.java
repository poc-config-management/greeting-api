package poc.config.managemnt.greeting;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;

import static java.util.Objects.requireNonNull;


@Slf4j
@SpringBootApplication
public class  GreetingApp {

    public static void main(final String[] args) {
        SpringApplication.run(GreetingApp.class, args);
    }



    record GreetingMessage(String date, String tenant, String message) {}

    @Service
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    static class GreetingService {

        Properties.TenantsPropertiesWrapper config;

        public GreetingService(@NonNull Properties.TenantsPropertiesWrapper config) {
            this.config = config;
            log.info("service created");
        }

        GreetingMessage greeting(String tenant) {
            return config.tenantConfig(tenant)
                    .map(tenantConfig -> greet(tenant, tenantConfig.greeting(), tenantConfig.timezone()))
                    .orElseThrow(() -> new NotFoundException(String.format("Tenant %s not found", tenant)));
        }
        private GreetingMessage greet(String tenant, String greeting, ZoneId timezone) {
            var date = ZonedDateTime.now(timezone);
            var message = String.format("%s %s %s", config.prefix(), greeting, config.suffix());
            return new GreetingMessage(date.toString(), tenant, message);
        }

    }
    @RestController
    @AllArgsConstructor
    static class GreetingController {


        private final GreetingService greetingService;
        @GetMapping("tenants/{tenant}/greeting")
        public GreetingMessage greeting(@PathVariable String tenant) {
            return greetingService.greeting(tenant);
        }
    }

    @RestControllerAdvice
    static class GlobalExceptionHandler {

        @ExceptionHandler(NotFoundException.class)
        public ProblemDetail handleNotFoundException(NotFoundException e) throws URISyntaxException {
            ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
            problemDetail.setType(new URI("https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/404"));
            return problemDetail;
        }
    }

}
