package poc.config.managemnt.greeting;

import lombok.extern.slf4j.Slf4j;

import java.time.ZoneId;

@Slf4j
record TenantConfig (String greeting, ZoneId timezone){

    public TenantConfig {
        log.info("constructor: greeting={}, timezone={}", greeting, timezone);
    }
}