package poc.config.managemnt.greeting;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;

@Slf4j
public class Properties {

    @ConfigurationProperties(prefix = "app")
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class TeanantsProperties {

        Map<String, TenantConfig> tenants;

        public Map<String, TenantConfig> getTenants() {
            log.info("getTenants: {}", tenants);
            return null;
        }

        public Map<String, TenantConfig> tenants() {
            log.info("tenants: {}", tenants);
            return Map.copyOf(tenants);
        }

        public void setTenants(Map<String, TenantConfig> tenants) {
            log.info("setTenants: {}", tenants);
            this.tenants = tenants;
        }
    }


    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    public static class TenantsPropertiesWrapper {
        TeanantsProperties teanantsProperties;
        CommonConfigurations commonConfigurations;

        public TenantsPropertiesWrapper(TeanantsProperties teanantsProperties, CommonConfigurations commonConfigurations) {
            this.teanantsProperties = teanantsProperties;
            this.commonConfigurations = commonConfigurations;

            log.info("Prefix: {}", prefix());
            log.info("Suffix: {}", suffix());
            forEachTenant((tenant, tenantConfig) -> log.info("{}: {}", tenant, tenantConfig));
        }

        public Optional<TenantConfig> tenantConfig(String tenant) {
            return Optional.ofNullable(teanantsProperties.getTenants().get(tenant));
        }

        public void forEachTenant(BiConsumer<? super String, ? super TenantConfig> action) {
            teanantsProperties.tenants.forEach(action);
        }

        public String prefix() {
            return commonConfigurations.getPrefix();
        }

        public String suffix() {
            return commonConfigurations.getSuffix();
        }

    }


    @ConfigurationProperties(prefix = "common")
    @FieldDefaults(level = AccessLevel.PRIVATE)
    @Data
    static class CommonConfigurations {
        String prefix;
        String suffix;
    }
}
