package poc.config.managemnt.greeting;

public class NotFoundException extends RuntimeException {
    public NotFoundException(String format) {
        super(format);
    }
}
