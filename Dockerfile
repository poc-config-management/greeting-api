# Start with a base image containing Java runtime (JDK 21 in this case)
FROM openjdk:21

# The application's jar file (replace the <name_of_your_jar_file> accordingly)
ARG JAR_FILE=target/greeting-api-0.0.1.jar

# cd into the /tmp directory
WORKDIR /app

# Copy the application's jar to the container
COPY ${JAR_FILE} application.jar

EXPOSE 8080

# Run the jar file
ENTRYPOINT ["java","-jar","application.jar"]
